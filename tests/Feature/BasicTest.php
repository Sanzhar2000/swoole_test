<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use SwooleTW\Http\Websocket\Websocket;
use Tests\TestCase;

class BasicTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function swoole_test()
    {
        $this->assertTrue(Websocket::loginUsingId(1));
        $this->assertTrue(Websocket::loginUsingId(2));

        $this->assertTrue(Websocket::toUserId(2)->emit('message', 'Hi there'));
        $this->assertTrue(Websocket::toUserId(1)->emit('message', 'Hi'));

    }
}
