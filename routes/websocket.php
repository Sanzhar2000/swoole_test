<?php


use Illuminate\Http\Request;
use SwooleTW\Http\Websocket\Facades\Websocket;
use SwooleTW\Http\Websocket\Facades\Room;
use App\Models\Room as RoomModel;
use App\Http\Middleware\MyOwnAuthMiddleware;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Websocket Routes
|--------------------------------------------------------------------------
|
| Here is where you can register websocket events for your application.
|
*/

Websocket::on('connect', function ($websocket, Request $request){

    if($request->myOwnUser != null){
        $user = $request->myOwnUser;
        $user_id = $user->id;

        Websocket::loginUsingId($user_id);

        echo 'We have a new user ' . Websocket::getUserId() . "\n";
    }else {
        echo 'NO AUTH USER' . "\n";
    }

})->middleware(MyOwnAuthMiddleware::class);

Websocket::on('disconnect', function ($websocket){
    $user_id = Websocket::getUserId();

    if($user_id == null){
        echo 'User ID ' .'user_id not available' . " has left.\n";
    }else{
        echo 'User ID ' . Websocket::getUserId() . " has left.\n";
    }
});

Websocket::on('sendmsg', function ($websocket, $data){
    $to = $data["to"];
    $message = $data["message"];

    Websocket::toUserId($to)->emit('message', $message);

    $json = addslashes(json_encode([
        'user1' => 'php',
        'user2' => 'swoole'
    ]));

    Websocket::toUserId($to)->emit('testjson', $json);
});

Websocket::on('broadcast', function ($websocket, $data){
    $message = $data["message"];

    Websocket::broadcast()->emit('message', $message);
});

Websocket::on('join_room', function ($websocket, $data){
    $room = $data["room"];

    Websocket::join($room);

    //RoomModel::create($data);
    $new_room = new RoomModel();
    $new_room->user_id = Websocket::getUserId();
    $new_room->name = $room;
    $new_room->save();

    $user = User::find(Websocket::getUserId());
    $user_list = RoomModel::where('name', $room)->with('user_id')->get();

    $json = addslashes(json_encode([
        'user' => $user,
        'user_list' => $user_list
    ]));

    Room::add(Websocket::getUserId(), $room);

    Websocket::to($room)->emit('joined', $json);

    var_dump(Room::getClients($room));
});

Websocket::on('send_room_msg', function ($websocket, $data){
    $room = $data["room"];
    $message = $data["message"];
    $user = User::find(Websocket::getUserId());

    $json = addslashes(json_encode([
        'user' => $user,
        'message' => $message
    ]));

    Websocket::to($room)->emit('new_msg', $json);

});

Websocket::on('leave_room', function ($websocket, $data){
    $room = $data["room"];

    $user = User::find(Websocket::getUserId());
    $json = addslashes(json_encode([
        'user' => $user,
    ]));

    Websocket::to($room)->emit('user_leave_room', $json);

    RoomModel::where('user_id', Websocket::getUserId())->where('name', $room)->forceDelete();

    Websocket::leave($room);
    Room::delete(Websocket::getUserId(), $room);
});
