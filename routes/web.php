<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('room/{roomname}', function ($roomname){
    return view('messanger', [
        'roomname' => $roomname
    ]);
});

Route::get('/messanger', function (){
    return view('messanger');
});

Route::get('/user', function () {
    return auth()->user();
});
