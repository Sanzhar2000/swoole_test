<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MyOwnAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->wsToken != ''){
            $user = User::where('wsToken', $request->wsToken)->first();
            if (isset($user)){
                $request->replace(['myOwnUser' => $user]);
            }
        }

        return $next($request);
    }
}
