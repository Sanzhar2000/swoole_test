<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(env('APP_ENV') != 'production')
        {
            $first_names = [
                'Nursultan',
                'Vladimir',
                'Donald',
                'Joe',
            ];

            $last_names = [
                'Nazarbayev',
                'Putin',
                'Trump',
                'Byden',
            ];

            $emails = [
                'nursultan.nazarbayev@gmail.com',
                'vladimir.putin@gmail.com',
                'donald.trump@gmail.com',
                'joe.byden@gmail.com',
            ];

            $password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';

            for ($i = 0; $i < 4; $i++)
            {
                $users[] = [
                    'first_name' => $first_names[$i],
                    'last_name' => $last_names[$i],
                    'phone' => '8707707010' . $i,
                    'email' => $emails[$i],
                    'wsToken' => null,
                    'password' => $password,
                    'remember_token' => Str::random(10),
                ];
            }

            User::insert($users);
        }

    }
}
