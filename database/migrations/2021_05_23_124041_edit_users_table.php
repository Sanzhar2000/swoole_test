<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('name');

            $table->string('first_name')->after('id')->default(null)->nulllable();
            $table->string('last_name')->after('first_name')->default(null)->nulllable();
            $table->string('phone')->after('last_name')->default(null)->nulllable();

            $table->string('wsToken')->nullable()->after('last_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
