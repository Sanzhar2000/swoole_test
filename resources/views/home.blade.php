@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <textarea name="broadcast" id="broadcast"></textarea>
                    <button id="send" onclick="broadcast();">Send Broadcast</button>
                    <hr />
                    <input type="text" name="to" id="to">
                    <textarea name="message" id="message"></textarea>
                    <button id="send" onclick="sendmsg();">Send Message</button>

                    <div id="messagecontent"></div>

{{--                    {{ __('You are logged in!') }}--}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
