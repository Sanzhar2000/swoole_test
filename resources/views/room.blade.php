@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <input type="text" name="room" value="{{ $roomname }}" id="to">
                    <textarea name="message" id="message"></textarea>
                    <button id="send" onclick="send_to_room();">Send Message</button>
                    <hr />

                    <div id="messagecontent"></div>

{{--                    {{ __('You are logged in!') }}--}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
