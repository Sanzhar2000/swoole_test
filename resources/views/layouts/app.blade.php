<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

{{--    <script src="https://cdn.socket.io/socket.io-1.3.4.js"></script>--}}
{{--    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>--}}
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
{{--<script type="text/javascript">--}}
{{--    let socket = io.connect('ws://127.0.0.1:1215{{ Auth::user() ? '?wsToken='.Auth::user()->wsToken : '' }}', {transports: ['websocket']});--}}

{{--    socket.on('message', function (data){--}}
{{--        document.getElementById('messagecontent').innerHTML += '<h1>'+data+'</h1>';--}}
{{--    });--}}

{{--    socket.on('testjson', function (data){--}}
{{--        let myobj = JSON.parse(data);--}}
{{--        console.log(myobj.user1);--}}
{{--    });--}}

{{--    function sendmsg()--}}
{{--    {--}}
{{--        let message = document.getElementById('message').value;--}}
{{--        let to = document.getElementById('to').value;--}}
{{--        socket.emit('sendmsg', {--}}
{{--            to: to,--}}
{{--            message: message--}}
{{--        });--}}
{{--    }--}}

{{--    function broadcast()--}}
{{--    {--}}
{{--        let message = document.getElementById('broadcast').value;--}}

{{--        socket.emit('broadcast', {--}}
{{--            message: message--}}
{{--        });--}}
{{--    }--}}

{{--    let url = window.location.href;--}}
{{--    let newURL = url.substring(url.padStart, url.length);--}}

{{--    if (newURL.startsWith('http://127.0.0.1:1215/room')) {--}}
{{--        socket.emit('join_room', {--}}
{{--            room: '{{ $roomname ?? '' }}'--}}
{{--        });--}}
{{--    }--}}

{{--    function send_to_room()--}}
{{--    {--}}
{{--        let message = document.getElementById('message').value;--}}
{{--        let room = document.getElementById('room').value;--}}

{{--        socket.emit('send_to_room', {--}}
{{--            room: room,--}}
{{--            message: message--}}
{{--        });--}}
{{--    }--}}
{{--</script>--}}
