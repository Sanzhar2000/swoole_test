<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <script src="https://use.fontawesome.com/45e03a14ce.js"></script>


    {{--    <script src="https://cdn.socket.io/socket.io-1.3.4.js"></script>--}}
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>

    <!-- Styles -->
    <link href="{{ asset('assets/css/messanger.css') }}" rel="stylesheet">
</head>
<body>
    <div class="main_section">
        <div class="container">
            <div class="chat_container">
                <div class="col-sm-3 chat_sidebar">
                    <div class="row">
                        <div id="custom-search-input">
                            <div class="input-group col-md-12">
                                <input type="text" class="  search-query form-control" placeholder="Conversation" />
                                <button class="btn btn-danger" type="button">
                                    <span class=" glyphicon glyphicon-search"></span>
                                </button>
                            </div>
                        </div>
                        <div class="dropdown all_conversation">
                            <button class="dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-weixin" aria-hidden="true"></i>
                                All Conversations
                                <span class="caret pull-right"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                <li><a href="#"> All Conversation </a>  <ul class="sub_menu_ list-unstyled">
                                        <li><a href="#"> All Conversation </a> </li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                        <div class="member_list">
                            <ul class="list-unstyled" id="userlist">

                            </ul>
                        </div></div>
                </div>
                <!--chat_sidebar-->


                <div class="col-sm-9 message_section">
                    <div class="row">
                        <div class="new_message_head">
                            <div class="pull-left"><button><i class="fa fa-plus-square-o" aria-hidden="true"></i> New Message</button></div><div class="pull-right"><div class="dropdown">
                                    <button class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-cogs" aria-hidden="true"></i>  Setting
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Profile</a></li>
                                        <li><a href="#">Logout</a></li>
                                    </ul>
                                </div></div>
                        </div><!--new_message_head-->

                        <div class="chat_area">
                            <ul class="list-unstyled" id="msgroom">

                            </ul>
                        </div><!--chat_area-->
                        <div class="message_write">
                            <textarea class="form-control room_message" placeholder="type a message"></textarea>
                            <div class="clearfix"></div>
                            <div class="chat_bottom"><a href="#" class="pull-left upload_btn"><i class="fa fa-cloud-upload" aria-hidden="true"></i>
                                    Add Files</a>
                                <a href="#" class="pull-right btn btn-success send_text">
                                    Send</a></div>
                        </div>
                    </div>
                </div> <!--message_section-->
            </div>
        </div>
    </div>
</body>
</html>
<script type="text/javascript">
    let socket = io.connect('ws://127.0.0.1:1215{{ Auth::user() ? '?wsToken='.Auth::user()->wsToken : '' }}', {transports: ['websocket']});

    socket.on('joined', function (data){
        let joined = JSON.parse(data);

        // $.each(joined.user_list, function (key, value){
        //     console.log(value);
        // });

        document.getElementById('msgroom').innerHTML += '<li class="left clearfix">'+
            '<span class="chat-img1 pull-left">'+'<i class="fa fa-user fa-4x"></i>'+'</span>'+'<div class="chat-body1 clearfix">'+
            '<p>'+joined.user.first_name+' has joined'+'</p>'+'</div>'+'</li>';

        $.each(joined.user_list, function (key, value){
            let user = value.user_id;

            if(!$('.users').hasClass('user_'+user.id)){
                document.getElementById('userlist').innerHTML += '<li class="left users user_'+user.id+' clearfix" id="user_'+joined.user.id+'">'+
                    '<span class="chat-img pull-left">'+'<i class="fa fa-user fa-2x"></i>'+'</span>'+'<div class="chat-body clearfix">'+
                    '<div class="header_sec">'+'<strong class="primary-font">'+user.first_name+'</strong><strong class="pull-right">'+
                    '09:45AM</strong>'+'</div>'+'</div>'+'</li>';
            }

        });
    });

    function send_room_msg(){
        let message = $('.room_message').val();
        socket.emit('send_room_msg', {
            message: message,
            room: '{{ $roomname }}'
        });

        $('.room_message').val('');
        return false;
    }

    let url = window.location.href;
    let newURL = url.substring(url.padStart, url.length);

    if (newURL.startsWith('http://127.0.0.1:1215/room')) {
        socket.emit('join_room', {
            room: '{{ $roomname ?? '' }}'
        });

        socket.on('user_leave_room', function (data){
            let leave = JSON.parse(data);

            document.getElementById('msgroom').innerHTML += '<li class="left clearfix">'+
                '<span class="chat-img1 pull-left">'+'<i class="fa fa-user fa-4x"></i>'+'</span>'+'<div class="chat-body1 clearfix">'+
                '<p>'+leave.user.first_name+' has left'+'</p>'+'</div>'+'</li>';

            if(!$('.users').hasClass('user_'+leave.user.id)) {
                $('.user_'+leave.user.id).remove();

            }
        });

        $(document).on('click', '.send_text', function (){
            send_room_msg();
        });

        $(document).on('keypress', '.room_message', function (event){
            if(event.keyCode == '13'){
                send_room_msg();
            }
        });
    }

    socket.on('new_msg', function (data){
        let room_msg = JSON.parse(data);

        document.getElementById('msgroom').innerHTML += '<li class="left clearfix">'+
            '<span class="chat-img1 pull-left">'+'<i class="fa fa-user fa-4x"></i>'+'</span>'+'<div class="chat-body1 clearfix">'+
            '<p>'+room_msg.user.first_name+': '+room_msg.message+'</p>'+'</div>'+'</li>';
    });

    window.onbeforeunload = function (){
        socket.emit('leave_room', {
            room: '{{ $roomname ?? '' }}'
        });
    };
</script>
